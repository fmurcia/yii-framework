<html lang="es">
    <head>
        <title>Telesentinel | Monitoreo de alarmas televideo y rastreo vehícular | Inicio </title>
        <link rel="stylesheet" href="https://www.telesentinel.com/site/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://www.telesentinel.com/site/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://www.telesentinel.com/site/css/styler.css" />
        <link rel="stylesheet" href="https://www.telesentinel.com/site/css/responsive.css" />
        <link rel="shortcut icon" href="img/favicon.ico" />
    </head>
    <body >
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="index.html"><img src="https://www.telesentinel.com/site/img/logo.png" alt="" /></a>
                </div>
            </div>
        </nav>
        <div class=" container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12"><img src="https://www.telesentinel.com/site/img/404.jpg" alt="" style="width: 100%"/></div>
                </div>
            </div>
        </div>
    </body>
</html>

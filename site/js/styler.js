$(".lnksupport").click(function () {
    window.open('http://dev.telesentinel.info:1440/TeamViewerQS.zip');
});
$(".lnkemail").click(function () {
    window.open('http://mail.telesentinel.com', '_blank');
});
$(".lnkweb").click(function () {
    window.open('https://www.telesentinel.com/web', '_blank');
});
$(".lnkblog").click(function () {
    window.open('https://www.telesentinel.com/blog/noticias.php', '_self');
});
$(".iconsmartsense").click(function () {
    window.open('https://www.telesentinel.com/smartsense', '_blank');
}); 
$(".equipos").click(function () {
    window.location.href = "equipos.php";
});
$(".cobertura").click(function () {
    window.location.href = "cobertura.php";
});
$(".atencion").click(function () {
    window.open('https://www.telesentinel.com/sac/pqr.html', '_blank');
});
$(".trabajosite").click(function () {
    window.open('https://www.telesentinel.com/sac/trabajo.html', '_blank');
});

$(".iconpse").click(function () {
    window.open('https://www.telesentinel.com/pagoonline/', '_blank');
});
$(".iconventas").click(function () {
    window.open('https://www.telesentinel.com/web', '_blank');
});
$(".iconemail").click(function () {
    window.open('http://mail.telesentinel.com', '_blank');
});

$(".psesite").click(function () {
    window.open('https://www.telesentinel.com/pagoonline/', '_blank');
});
$(".mapa").click(function () {
    window.location.href = "sitemap.html";
});
$(".pagos").click(function () {
    window.open('http://www.telesentinel.com/pagoonline/', '_blank');
});
$(".clientes").click(function () {
    alert("En Construccion");
});

$(".rastreo").click(function () {
    window.open('http://telesentinel.servertrack.co/telesentinel/index.php', '_blank');
});

$(".inicioland").click(function () {
    window.open('https://www.telesentinel.com/site/?campania=registroseo&ciudad=11001', '_self');
});

$(".rastreoland").click(function () {
    window.open('https://www.telesentinel.com/site/rastreo_vehicular.html?campania=registroseo&ciudad=11001', '_self');
});

$(".smartland").click(function () {
    window.open('https://www.telesentinel.com/smartland/?campania=registroseo&ciudad=11001', '_self');
});

$(".videoland").click(function () {
    window.open('https://www.telesentinel.com/site/televideo.html?campania=registroseo&ciudad=11001', '_self');
});

$(".contactenos").click(function () {
    $('html,body').animate({scrollTop: $('#contact').offset().top}, 'slow');
});

$(".pol1").click(function () {
    window.open('https://www.telesentinel.com/files/PoliticaTelesentinel.pdf', '_blank');
});
$(".pol2").click(function () {
    window.open('https://www.telesentinel.com/files/PoliticaTratamientoDatos.pdf', '_blank');
});
$(".pol3").click(function () {
    window.open('https://www.telesentinel.com/files/PoliticaTratamientoDatos_com.pdf', '_blank');
});
$(".pol4").click(function () {
    window.open('https://www.telesentinel.com/files/PoliticaTratamientoDatos_Soluciones.pdf', '_blank');
});
$(".pol5").click(function () {
    window.open('https://www.telesentinel.com/files/PortafoliodeServiciosTelesentinel.pdf', '_blank');
});
$(".pol6").click(function () {
    window.open('https://www.telesentinel.com/files/Politicadecookies.pdf', '_blank');
});
$(".pol7").click(function () {
    window.open('https://www.telesentinel.com/files/Politica_SIPLAFT_Telesentinel.pdf', '_blank');
});
$(".rastreovh").click(function () {
    window.open('http://telesentinel1.servertrack.co/telesentinelsv/', '_blank');
});
$(".iconfacebook").click(function () {
    window.open('https://www.facebook.com/TelesentinelOficial/', '_blank');
});
$(".iconyoutube").click(function () {
    window.open('https://www.youtube.com/c/TelesentinelMonitoreodealarmasBogot%C3%A1', '_blank');
});
$(".iconinstagram").click(function () {
    window.open('https://www.instagram.com/telesentinel_colombia/', '_blank');
});

$('.solo-numero').keyup(function () {
    this.value = (this.value + '').replace(/[^0-9]/g, '');
});

function conversion(form) {

    var formulario  = $('#' + form).serialize();
    $.ajax({
        type: "POST",
        url: 'https://www.telesentinel.com/telemark/index.php/conversion/site',
        data: formulario,
        beforeSend: function () {
            swal({
                position: 'center',
                type: 'info',
                title: 'Un Momento por favor',
                showConfirmButton: false,
                timer: 2000
            });
        },
        error: function (request, error) {
            if (error == "timeout") {
                swal({
                    position: 'center',
                    type: 'danger',
                    title: 'Aplicacion en mantenimiento intentenlo mas tarde. Gracias',
                    showConfirmButton: false
                });
            } else {
                swal({
                    position: 'center',
                    type: 'warning',
                    title: 'Intentenlo mas tarde. Gracias',
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        success: function (request) {
	    if (request == 'ok') {
                $('#' + form)[0].reset();
                window.location = "https://www.telesentinel.com/gracias/";
            } else {
                swal({
                    position: 'center',
                    type: 'warning',
                    title: 'Aplicacion en mantenimiento intentenlo mas tarde. Gracias',
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        }
    });
    return false;
}

$(function () {
    $('#carousel-example-generic').carousel();
});